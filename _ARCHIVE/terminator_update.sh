#!/bin/bash
PATH=/usr/local/bin:/usr/bin:/bin
cd /data/project/wikidata-terminator

for lang in ta kn cy am eo ht or nap ast gu te ar ca ml nl ru en de fr es it hr uz he fi sv fa ro da cs pl zh hi nb ; do
#for lang in ta ; do

echo "select t1.term_text AS label,count(*) as cnt from wb_terms t1 where t1.term_language='$lang' and t1.term_entity_type='item' and t1.term_type='label' and not exists (SELECT * FROM wb_terms t2 WHERE t1.term_full_entity_id=t2.term_full_entity_id and t2.term_language='$lang' and t2.term_entity_type='item' and t2.term_type='description') group by t1.term_text having cnt>1 order by cnt desc" | /usr/bin/mysql --defaults-file=/data/project/wikidata-terminator/replica.my.cnf -h wikidatawiki.labsdb wikidatawiki_p > /data/project/wikidata-terminator/data/wikidata.$lang.todo2
rm -f /data/project/wikidata-terminator/data/wikidata.$lang.todo
mv /data/project/wikidata-terminator/data/wikidata.$lang.todo2 /data/project/wikidata-terminator/data/wikidata.$lang.todo
head -1000 /data/project/wikidata-terminator/data/wikidata.$lang.todo > /data/project/wikidata-terminator/data/wikidata.$lang.todo.1000


echo "select pl_title,count(*) as cnt from pagelinks where pl_namespace=0 and not exists ( SELECT * FROM wb_terms WHERE term_language='$lang' and term_entity_type='item' and term_type='label' and pl_title=term_full_entity_id and pl_namespace=0 limit 1) group by pl_title having cnt>4 order by cnt desc" | /usr/bin/mysql --defaults-file=/data/project/wikidata-terminator/replica.my.cnf -h wikidatawiki.labsdb wikidatawiki_p > /data/project/wikidata-terminator/data/wikidata.$lang.t1000.tmp
rm -f /data/project/wikidata-terminator/data/wikidata.$lang.t1000
mv /data/project/wikidata-terminator/data/wikidata.$lang.t1000.tmp /data/project/wikidata-terminator/data/wikidata.$lang.t1000
head -1000 /data/project/wikidata-terminator/data/wikidata.$lang.t1000 > /data/project/wikidata-terminator/data/wikidata.$lang.t1000.1000


echo "select ips_item_id,count(*) AS cnt FROM wb_items_per_site ips where not exists (SELECT * FROM wb_items_per_site ips2 where ips.ips_item_id=ips2.ips_item_id and ips2.ips_site_id='${lang}wiki') group by ips_item_id order by cnt desc limit 100000" | /usr/bin/mysql --defaults-file=/data/project/wikidata-terminator/replica.my.cnf -h wikidatawiki.labsdb wikidatawiki_p > /data/project/wikidata-terminator/data/wikidata.$lang.tx.tmp
rm -f /data/project/wikidata-terminator/data/wikidata.$lang.tx.1000
mv /data/project/wikidata-terminator/data/wikidata.$lang.tx.tmp /data/project/wikidata-terminator/data/wikidata.$lang.tx
head -1000 /data/project/wikidata-terminator/data/wikidata.$lang.tx > /data/project/wikidata-terminator/data/wikidata.$lang.tx.1000

done

chmod g+rw data/*

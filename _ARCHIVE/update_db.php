#!/usr/bin/php
<?PHP

/* No longer used? */

require_once ( '/data/project/wikidata-terminator/public_html/php/common.php' ) ;

$languages = [ 'am','ar','ast','ca','cs','cy','da','de','en','eo','es','fa','fi','fr','gu','he','hi','hr','ht','it','kn','ml','nap','nb','nl','or','pl','ro','ru','sv','te','uz','zh' ] ;

function flushOut () {
	global $db_tool , $sql_command , $table ;
	if ( count($sql_command) == 0 ) return ;
	$sql_command = "INSERT INTO $table (item,missing_languages,missing_count,random) VALUES " . implode(',',$sql_command ) ;
	if(!$result = $db_tool->query($sql_command)) die('There was an error running the query [' . $db_tool->error . ']'."\n$sql_command\n\n");
	$sql_command = [] ;
}

function flushCache ($new_item) {
	global $last_item , $language_cache , $languages , $sql_command , $db ;
	if ( $last_item != '' ) {
		$missing = count($languages) - count($language_cache) ;
		if ( $missing > 0 and $missing < count($language_cache) ) {
			$todo = [] ;
			foreach ( $languages AS $l ) {
				if ( !isset($language_cache[$l]) ) $todo[] = $l ;
			}
			$sql_command[] = "('{$last_item}','" . $db->real_escape_string(','.implode(',',$todo).',') . "',$missing,rand())" ;
		}
	}
	$last_item = $new_item ;
	$language_cache = [] ;
	if ( count($sql_command) > 1000 ) flushOut() ;
}


$db_tool = openToolDB ( 'terminator_p' ) ;
$table = 'missing_labels' ;
$sql = "TRUNCATE $table" ;
if(!$result = $db_tool->query($sql)) die('There was an error running the query [' . $db_tool->error . ']'."\n$sql\n\n");


$sql_command = [] ;
$last_item = '' ;
$language_cache = [] ;
$db = openDB ( 'wikidata' , 'wikidata' ) ;
$sql = "SELECT DISTINCT
	wbxl_language as term_language,
	concat('Q',wbit_item_id) AS term_full_entity_id
FROM wbt_item_terms
	INNER JOIN wbt_term_in_lang ON wbit_term_in_lang_id = wbtl_id AND wbtl_type_id = 1
	INNER JOIN wbt_text_in_lang ON wbtl_text_in_lang_id = wbxl_id AND wbxl_language IN ('" . implode("','",$languages) . "')
ORDER BY wbit_item_id" ;
if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']'."\n$sql\n\n");
print "Processing...\n" ;
while($o = $result->fetch_object()){
	if ( $last_item != $o->term_full_entity_id ) flushCache($o->term_full_entity_id) ;
	$language_cache[$o->term_language] = 1 ;
}
flushCache('') ;
flushOut() ;

/*
$sql = "CREATE TABLE `missing_labels` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `item` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `missing_languages` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `missing_count` int(11) NOT NULL,
  `random` float NOT NULL,
  PRIMARY KEY (`id`),
  KEY `random_ml` (`random`,`missing_languages`,`missing_count`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci" ;
if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']'."\n$sql\n\n");
*/


?>
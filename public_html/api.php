<?php

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR); // E_ALL|
ini_set('display_errors', 'On');
ini_set('memory_limit','500M');
set_time_limit ( 60 * 10 ) ; // Seconds

header('Content-type: application/json');

require_once ( '/data/project/wikidata-terminator/scripts/terminator.php' ) ;

$tm = new Terminator ;

$action = $tm->tfc->getRequest ( 'action' , '' ) ;

$out = [ 'status' => 'OK' ] ;

if ( $action == 'languages' ) {
	$out['languages'] = $tm->getAllLanguages() ;

} else if ( $action == 'run_query' ) {
	$type = $tm->tfc->getRequest ( 'type' , 'labels' ) ;
	$languages = explode ( ',' , $tm->tfc->getRequest ( 'languages' , 'en' ) ) ;
	$present = explode ( ',' , $tm->escape ( $tm->tfc->getRequest ( 'present' , '' ) ) ) ;
	$sort_by = $tm->tfc->getRequest ( 'sort_by' , 'score' ) ;
	$sui = $tm->tfc->getRequest ( 'sui' , 0 ) * 1 ;
	$max_results = $tm->tfc->getRequest ( 'max_results' , '50' ) * 1 ;
	$sparql = trim ( $tm->tfc->getRequest ( 'sparql' , '' ) ) ;
	$offset = $tm->tfc->getRequest ( 'offset' , 0 ) * 1 ;
	$mode = $tm->tfc->getRequest ( 'mode' , 'top' ) ;

	$sparql_results = '' ;
	if ( $sparql != '' ) {
		$sparql_results = str_replace ( 'Q' , '' , implode ( ',' , $tm->tfc->getSPARQLitems ( $sparql ) ) ) ;
	}

	$column = '`' . $tm->escape($type) . '`' ;

	$conditions = [] ;

	$cl = [] ;
	foreach ( $languages AS $l ) {
		$l = $tm->escape ( strtolower ( trim ( $l ) ) ) ;
		if ( $l != '' ) $cl[] = "{$column} NOT LIKE '%,{$l},%'" ;
	}
	if ( count($cl) > 0 ) $conditions[] = '((' . implode ( ') OR (',$cl) . '))' ;

	$cl = [] ;
	foreach ( $present AS $l ) {
		$l = $tm->escape ( strtolower ( trim ( $l ) ) ) ;
		if ( $l != '' ) $cl[] = "{$column} LIKE '%,{$l},%'" ;
	}
	if ( count($cl) > 0 ) $conditions[] = '((' . implode ( ') OR (',$cl) . '))' ;

	if ( !$sui ) $conditions[] = "p31 NOT IN (SELECT main_p31.q FROM main_p31)" ;

	if ( $sparql_results != '' ) $conditions[] = "q IN ({$sparql_results})" ;

	$found = 0 ;
	$rand = rand()/getrandmax() + 0.1 ;
	while ( $found == 0 ) {
		$sql = "SELECT * FROM items WHERE (" . implode ( ') AND (' , $conditions ) . ")" ;

		if ( $mode == 'game' ) {
			$rand -= 0.1 ;
			$sql .= " AND random>={$rand} ORDER BY random LIMIT 1" ;
		} else {
			$sql .= " ORDER BY `" . $tm->escape($sort_by) . "` DESC LIMIT {$max_results}" ;
			if ( $offset > 0 ) $sql .= " OFFSET {$offset}" ;
		}

		if ( $sparql == '' ) $out['sql'] = $sql ;
		$out['results'] = [] ;

		$result = $tm->getSQL ( $sql ) ;
		while($o = $result->fetch_object()){
			$found++ ;
			$out['results'][] = $o ;
		}
		if ( $mode == 'game' and $rand < 0 ) break ; // No results, ever
		if ( $mode == 'top' and $found == 0 ) break ; // No hope
	}
	$offset += $found ;
	$out['offset'] = $offset ;

} else {
	require_once '/data/project/magnustools/public_html/php/Widar.php' ;
	$widar = new \Widar ( 'wikidata-terminator' ) ;
	$widar->attempt_verification_auto_forward ( '/' ) ;
	$widar->authorization_callback = 'https://wikidata-terminator.toolforge.org/api.php' ;
	if ( $widar->render_reponse(true) ) exit(0);

	$out['status'] = "ERROR: bad action '{$action}'" ;
}

print json_encode ( $out ) ;

?>
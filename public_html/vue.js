'use strict';

let router ;
let app ;
let wd = new WikiData() ;

var languages = {} ;

$(document).ready ( function () {
    vue_components.toolname = 'wikidata-terminator' ;
//    vue_components.components_base_url = 'https://tools.wmflabs.org/magnustools/resources/vue/' ; // For testing; turn off to use tools-static
    Promise.all ( [
        vue_components.loadComponents ( ['wd-date','wd-link','tool-translate','tool-navbar','commons-thumbnail','widar','autodesc',//,'typeahead-search','value-validator',
            'vue-components/main-page.html',
            'vue-components/language-typeahead.html',
            'vue-components/top-page.html',
            ] ) ,
        new Promise ( function(resolve, reject) {
            $.get ( './api.php' , {action:'languages'},function(d){
                languages = d.languages ;
                resolve() ;
            },'json')
        } )
    ] )
    .then ( () => {
        widar_api_url = 'https://wikidata-terminator.toolforge.org/api.php' ;
        wd_link_wd = wd ;

        const routes = [
            { path: '/', component: MainPage , props:true },
            { path: '/top', component: TopPage , props:true },
            { path: '/top/:orig_type', component: TopPage , props:true },
            { path: '/top/:orig_type/:orig_langs', component: TopPage , props:true },
        ] ;
        router = new VueRouter({routes}) ;
        app = new Vue ( { router } ) .$mount('#app') ;

    } ) ;
} ) ;

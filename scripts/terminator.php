<?php

require_once ( '/data/project/wikidata-terminator/public_html/php/ToolforgeCommon.php' ) ;

class Terminator {

	public $tfc ;
	public $dbt ;
	public $dbw ;
	public $min_score = 15 ;
	public $batch_size = 5000 ;

	public function __construct () {
		$this->tfc = new ToolforgeCommon ( 'wikidata-terminator' ) ;
	}

	public function getDB () {
		if ( !isset($this->dbt) ) $this->dbt = $this->tfc->openDBtool ( 'terminator_p' ) ;
		return $this->dbt ;
	}

	private function getDBwd () {
		if ( !isset($this->dbw) ) $this->dbw = $this->tfc->openDB ( 'wikidata' , 'wikidata' ) ;
		return $this->dbw ;
	}

	public function getSQL ( $sql ) {
		$db = $this->getDB() ;
		try {
			$ret = $this->tfc->getSQL ( $db , $sql ) ;
		} catch (Exception $e) { # Reconnect
			print "#FAILED TOOL:\n{$sql}\n\n";
			unset($this->dbt);
			$db = $this->getDB() ;
			$ret = $this->tfc->getSQL ( $db , $sql ) ;
		}
		return $ret ;
	}

	public function getSQLwd ( $sql ) {
		$db = $this->getDBwd() ;
		try {
			$ret = $this->tfc->getSQL ( $db , $sql ) ;
		} catch (Exception $e) { # Reconnect
			print "#FAILED WD:\n{$sql}\n\n";
			unset($this->dbw);
			$db = $this->getDBwd() ;
			$ret = $this->tfc->getSQL ( $db , $sql ) ;
		}
		return $ret ;
	}

	public function escape ( $s ) {
		return $this->getDB()->real_escape_string ( $s ) ;
	}

	public function getAllLanguages () {
		$ret = [] ;
		$star = '*' ;
		$url = 'https://www.wikidata.org/w/api.php?action=query&meta=siteinfo&siprop=languages&format=json' ;
		$j = json_decode ( file_get_contents($url) ) ;
		foreach ( $j->query->languages AS $l ) $ret[$l->code] = $l->$star ;
		return $ret ;
	}

	public function updateP31 ( $q_list = [] ) {
		# Read "main" p31 tags
		$main_p31 = [] ;
		$sql = "SELECT * FROM main_p31" ;
		$result = $this->getSQL ( $sql ) ;
		while($o = $result->fetch_object()) $main_p31[] = 'Q' . $o->q ;

		$q_start = 0 ;
		do {
			$sql = "SELECT q FROM items WHERE p31 IS NULL AND q>{$q_start}" ;
			if ( count($q_list) > 0 ) $sql .= " AND q IN (" . implode(',',$q_list) . ")" ;
			$sql .= " ORDER BY q LIMIT 100" ;
			$qs = [] ;
			$result = $this->getSQL ( $sql ) ;
			while($o = $result->fetch_object()){
				if ( $o->q > $q_start ) $q_start = $o->q ;
				$qs[] = $o->q ;
			}
			if ( count($qs) == 0 ) break ;
			$q2p31 = [] ;
			$sparql = "SELECT ?q ?p31 { VALUES ?q { wd:Q" . implode(' wd:Q',$qs) . " } . ?q wdt:P31 ?p31 }" ;
			$j = $this->tfc->getSPARQL ( $sparql ) ;
			if ( isset($j) and isset($j->results) and isset($j->results->bindings) ) {
				foreach ( $j->results->bindings AS $b ) {
					$q = $this->tfc->parseItemFromURL ( $b->q->value ) ;
					$p31 = $this->tfc->parseItemFromURL ( $b->p31->value ) ;
					if ( !isset($q2p31[$q]) or in_array($p31,$main_p31) ) $q2p31[$q] = $p31 ;
				}
			}
			foreach ( $q2p31 AS $q => $p31 ) {
				$this->getSQL ( "UPDATE items SET p31=".preg_replace('/\D/','',$p31).' WHERE q='.preg_replace('/\D/','',$q) ) ;
			}
		} while ( 1 ) ; # break in loop
	}

	public function updateDatabase ( $bespoke_sql = '' ) {
		$page_id_start = 0 ;
		if ( $bespoke_sql == '' ) {
			$result = $this->getSQL ( "SELECT * FROM `meta` WHERE `name`='last_page_id' LIMIT 1" ) ;
			if ( $o = $result->fetch_object() ) $page_id_start = $o->value * 1 ;
		}

		do {
			$sql = "SET SESSION group_concat_max_len = 1000000" ;
			$this->getSQLwd ( $sql ) ;

			$sql_labels = "
				SELECT group_concat(wbxl_language) FROM wbt_item_terms
				INNER JOIN wbt_term_in_lang ON wbit_term_in_lang_id = wbtl_id AND wbtl_type_id = 1 /* label */
				INNER JOIN wbt_text_in_lang ON wbtl_text_in_lang_id = wbxl_id
				WHERE wbit_item_id=substr(page_title,2)*1" ;
			$sql_descriptions = "
				SELECT group_concat(wbxl_language) FROM wbt_item_terms
				INNER JOIN wbt_term_in_lang ON wbit_term_in_lang_id = wbtl_id AND wbtl_type_id = 2 /* description */
				INNER JOIN wbt_text_in_lang ON wbtl_text_in_lang_id = wbxl_id
				WHERE wbit_item_id=substr(page_title,2)*1" ;

			$sql = "SELECT page_id,page_title,
			(SELECT pp_value*1 FROM page_props WHERE page_id=pp_page AND pp_propname='wb-claims') AS claims,
			(SELECT pp_value*1 FROM page_props WHERE page_id=pp_page AND pp_propname='wb-identifiers') AS identifiers,
			(SELECT pp_value*1 FROM page_props WHERE page_id=pp_page AND pp_propname='wb-sitelinks') AS sitelinks,
			({$sql_labels}) AS label_languages,
			({$sql_descriptions}) AS label_descriptions,
			(SELECT group_concat(regexp_replace(ips_site_id,'wiki$','')) FROM wb_items_per_site WHERE ips_item_id=regexp_replace(page_title,'^Q','')*1) AS sites
			FROM page
			WHERE page_namespace=0 AND page_id>{$page_id_start} AND page_is_redirect=0 " ;
			if ( $bespoke_sql != '' ) $sql .= " {$bespoke_sql} " ;
			$sql .= " HAVING claims+sitelinks-identifiers>={$this->min_score} ORDER BY page_id LIMIT {$this->batch_size}" ;
			
			$result = $this->getSQLwd ( $sql ) ;
			$number_of_results = 0 ;
			$sql_parts = [] ;
			$q_list = [] ;
			while($o = $result->fetch_object()){
				$number_of_results++ ;
				$q = preg_replace ( '/\D/' , '' , $o->page_title ) ;
				$claims = isset($o->claims) ? $o->claims : 0 ;
				$identifiers = isset($o->identifiers) ? $o->identifiers : 0 ;
				$sitelinks = isset($o->sitelinks) ? $o->sitelinks : 0 ;
				$score = $claims+$sitelinks-$identifiers ;
				$label_languages = $this->escape(','.$o->label_languages.',') ;
				$label_descriptions = $this->escape(','.$o->label_descriptions.',') ;
				$sites = $this->escape(','.$o->sites.',') ;
				$sql_parts[] = "({$q},{$claims},{$identifiers},{$sitelinks},'{$label_languages}','{$label_descriptions}','{$sites}',{$score},rand())" ;
				if ( $o->page_id > $page_id_start ) $page_id_start = $o->page_id ;
				$q_list[$q] = $q ;
			}
			if ( count($sql_parts) > 0 ) {
				$sql = "REPLACE INTO items (q,claims,identifiers,sitelinks,labels,descriptions,sites,score,random) VALUES " . implode ( ',' , $sql_parts ) ;
				$this->getSQL ( $sql ) ;
				$this->updateP31 ( $q_list ) ;
			}
			if ( $bespoke_sql == '' ) $this->getSQL("UPDATE `meta` SET `value`='$page_id_start' WHERE `name`='last_page_id'") ;
			else $number_of_results = -1 ;
		} while ( $number_of_results == $this->batch_size ) ;
		if ( $bespoke_sql == '' ) $this->getSQL ( "DELETE FROM items WHERE score<{$this->min_score}" ) ;
	}

	public function updateRC () {
		# Get last RC timestamp
		$last_rc_timestamp = '' ;
		$result = $this->getSQL ( "SELECT * FROM `meta` WHERE `name`='last_rc_timestamp' LIMIT 1" ) ;
		if ( $o = $result->fetch_object() ) $last_rc_timestamp = $o->value ;

		$qs = [] ;
		$qs_to_remove = [] ;

		# Get deleted items
		$sql = "SELECT DISTINCT log_title FROM logging WHERE log_action='delete' AND log_timestamp>='{$last_rc_timestamp}' AND log_namespace=0" ;
		$result = $this->getSQLwd ( $sql ) ;
		while ( $o = $result->fetch_object() ) {
			$qs_to_remove[$o->log_title] = $o->log_title ;
		}

		# Get Recent Changes
		$sql = "SELECT rc_title,page_is_redirect,max(rc_timestamp) AS ts FROM recentchanges,page WHERE page_id=rc_cur_id AND rc_namespace=0 AND rc_timestamp>='{$last_rc_timestamp}' GROUP BY rc_title" ;
		$result = $this->getSQLwd ( $sql ) ;
		while ( $o = $result->fetch_object() ) {
			if ( $last_rc_timestamp < $o->ts ) $last_rc_timestamp = $o->ts ;
			if ( $o->page_is_redirect ) $qs_to_remove[$o->rc_title] = $o->rc_title ;
			else $qs[] = $o->rc_title ;
		}

		# Remove deleted items/redirects
		if ( count($qs_to_remove) > 0 ) {
			foreach ( $qs_to_remove AS $k => $v ) $qs_to_remove[$k] = preg_replace ( '/\D/' , '' , $v ) ;
			$chunks = array_chunk ( $qs_to_remove , $this->batch_size ) ;
			unset($qs_to_remove);
			foreach ( $chunks AS $chunk ) {
				$sql = "DELETE FROM items WHERE q IN (" . implode(',',$chunk) . ")" ;
				$this->getSQL ( $sql ) ;
			}
		}

		# Update entries
		if ( count($qs) > 0 ) {
			$chunks = array_chunk ( $qs , $this->batch_size ) ;
			unset($qs);
			foreach ( $chunks AS $chunk ) {
				try {
					$this->updateDatabase ( " AND page_title IN ('" . implode("','",$chunk) . "')" ) ;
				} catch (Exception $e) {
					print_r($e);
				}
			}
			$this->getSQL ( "DELETE FROM items WHERE score<{$this->min_score}" ) ;
		}

		# Remember last RC timestamp
		$this->getSQL("UPDATE `meta` SET `value`='$last_rc_timestamp' WHERE `name`='last_rc_timestamp'") ;
	}

} ;

?>
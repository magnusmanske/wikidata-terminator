#!/usr/bin/php
<?php

require_once ( '/data/project/wikidata-terminator/scripts/terminator.php' ) ;

if ( !isset($argv[1]) ) die ( "command required\n" ) ;
$command = $argv[1] ;

$tm = new Terminator ;

#$languages = $tm->getAllLanguages() ;

if ( $command == 'update_p31' ) {
	$tm->updateP31() ;

} else if ( $command == 'update_all' ) {
	$tm->updateDatabase () ;

} else if ( $command == 'update_q' ) {
	$qs = explode ( ',' , preg_replace ( '/[^0-9,]/' , '' , $argv[2] ) ) ;
	$qs = "'Q" . implode ( "','Q" , $qs ) . "'" ;
	$tm->updateDatabase ( " AND page_title IN ({$qs})" ) ;

} else if ( $command == 'update_rc' ) {
	$tm->updateRC() ;

} else if ( $command == 'oneoff' ) {
	$qs = [] ;
	$result = $tm->getSQL ( "select q FROM items WHERE length(sites)=2000" ) ;
	while ( $o = $result->fetch_object() ) $qs[] = "'Q{$o->q}'" ;
	$qs = implode ( ',' , $qs ) ;
	$tm->updateDatabase ( " AND page_title IN ({$qs})" ) ;

} else {
	die ( "Unknown command: {$command}\n" ) ;
}

/*
pp_propname:
used: wb-claims,wb-identifiers,wb-sitelinks
not used: defaultsort,disambiguation,displaytitle,forcetoc,graph_specs,hiddencat,index,jsonconfig_getdata,kartographer,kartographer_frames,kartographer_links,newsectionlink,noeditsection,noindex,nonewsectionlink,notoc,page_image,page_image_free,page_top_level_section_count,staticredirect,templatedata,wikibase_item
*/

?>